/* 
  LEDMatrix V5 class by Aaron Liddiment (c) 2016
  modified:  Juergen Skrotzky (JorgenVikingGod@gmail.com)
  date:      2016/04/27
*/

#ifndef LEDMatrix_h
#define LEDMatrix_h

enum MatrixType_t { HORIZONTAL_MATRIX,
                    VERTICAL_MATRIX,
                    HORIZONTAL_ZIGZAG_MATRIX,
                    VERTICAL_ZIGZAG_MATRIX };

enum BlockType_t	{	HORIZONTAL_BLOCKS,
										VERTICAL_BLOCKS,
										HORIZONTAL_ZIGZAG_BLOCKS,
										VERTICAL_ZIGZAG_BLOCKS };

class cLEDMatrixBase
{
  friend class cSprite;

  protected:
    int16_t m_Width, m_Height;
    //struct CRGB *m_LED;
    struct CRGB m_OutOfBounds;

  public:
    cLEDMatrixBase();
    virtual uint32_t mXY(uint16_t x, uint16_t y)=0;
    void SetLEDArray(struct CRGB *pLED);	// Only used with externally defined LED arrays

    struct CRGB *m_LED;//AS: habe ich in public verschoben

    struct CRGB *operator[](int n);
    struct CRGB &operator()(int16_t x, int16_t y);
    struct CRGB &operator()(int16_t i);

    int Size()  { return(m_Width * m_Height); }
    int Width() { return(m_Width);  }
    int Height()  { return(m_Height); }

    void HorizontalMirror(bool FullHeight = true);
    void VerticalMirror();
    void QuadrantMirror();
    void QuadrantRotateMirror();
    void TriangleTopMirror(bool FullHeight = true);
    void TriangleBottomMirror(bool FullHeight = true);
    void QuadrantTopTriangleMirror();
    void QuadrantBottomTriangleMirror();

    void DrawPixel(int16_t x, int16_t y, CRGB Col);
    void DrawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, CRGB Col);
    void DrawRectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, CRGB Col);
    void DrawCircle(int16_t xc, int16_t yc, uint16_t r, CRGB Col);
    void DrawFilledRectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, CRGB Col);
    void DrawFilledCircle(int16_t xc, int16_t yc, uint16_t r, CRGB Col);
    //--------------
};

template<int16_t tMWidth, int16_t tMHeight, MatrixType_t tMType, int8_t tBWidth = 1, int8_t tBHeight = 1, BlockType_t tBType = HORIZONTAL_BLOCKS> class cLEDMatrix : public cLEDMatrixBase
{
  private:
    static const int16_t m_absMWidth = (tMWidth * ((tMWidth < 0) * -1 + (tMWidth > 0)));
    static const int16_t m_absMHeight = (tMHeight * ((tMHeight < 0) * -1 + (tMHeight > 0)));
    static const int16_t m_absBWidth = (tBWidth * ((tBWidth < 0) * -1 + (tBWidth > 0)));
    static const int16_t m_absBHeight = (tBHeight * ((tBHeight < 0) * -1 + (tBHeight > 0)));
    struct CRGB *p_LED;

  public:
    cLEDMatrix(bool doMalloc=true)
    {
      m_Width = m_absMWidth * m_absBWidth;
      m_Height = m_absMHeight * m_absBHeight;
      if (doMalloc) {
	  // On ESP32, there is more memory available via malloc than static global arrays
          p_LED = (struct CRGB *) malloc(m_absMWidth * m_absBWidth * m_absMHeight * m_absBHeight * sizeof(CRGB));
          m_LED = p_LED;
	  if (! p_LED) {
	     Serial.begin(115200);
	     Serial.println("Malloc LEDMatrix Failed");
	     while (1);
	  }
      } else {
	  Serial.println("LED array not intialized, must be set by SetLEDArray");
      }
    }
    void SetLEDArray(struct CRGB *pLED)
    {
      p_LED = pLED;
      m_LED = pLED;
    }


    //======================================================
    //========== Andres remapping function =================
    //======================================================
    const int MISSING_LED = 300;

    virtual uint32_t mXY(uint16_t x, uint16_t y) {

    	switch(y) {
    		case 0:
    			switch(x){
    				case 4: return 2;
    				case 5: return 1;
    				case 6: return 0;
    				default: return MISSING_LED;
    			}
    			break;
    		case 1:
    			switch(x){
    				case 2: return 3;
    				case 3: return 4;
    				case 4: return 5;
    				case 5: return 6;
    				case 6: return 7;
    				case 7: return 8;
    				default: return MISSING_LED;
    			}
    			break;
    		case 2:
    			switch(x){
    				case 1: return 16;
    				case 2: return 15;
    				case 3: return 14;
    				case 4: return 13;
    				case 5: return 12;
    				case 6: return 11;
    				case 7: return 10;
    				case 8: return 9;
    				case 16: return 26;
    				case 17: return 27;
    				case 18: return 28;
    				case 19: return 29;
    				default: return MISSING_LED;
    			}
    			break;
    		case 3:
    			switch(x){
    				case 1: return 17;
    				case 2: return 18;
    				case 3: return 19;
    				case 4: return 20;
    				case 5: return 21;
    				case 6: return 22;
    				case 7: return 23;
    				case 8: return 24;
    				case 9: return 25;
    				case 15: return 36;
    				case 16: return 35;
    				case 17: return 34;
    				case 18: return 33;
    				case 19: return 32;
    				case 20: return 31;
    				case 21: return 30;
    				default: return MISSING_LED;
    			}
    			break;
    		case 4:
    			switch(x){
    				case 0: return 56;
    				case 1: return 55;
    				case 2: return 54;
    				case 3: return 53;
    				case 4: return 52;
    				case 5: return 51;
    				case 6: return 50;
    				case 7: return 49;
    				case 8: return 48;
    				case 9: return 47;
    				case 10: return 46;
    				case 11: return 45;
    				case 12: return 44;
    				case 13: return 43;
    				case 14: return 42;
    				case 15: return 41;
    				case 16: return 40;
    				case 17: return 39;
    				case 18: return 38;
    				case 19: return 37;
    				default: return MISSING_LED;
    			}
    			break;
    		case 5:
    			switch(x){
    				case 0: return 57;
    				case 1: return 58;
    				case 2: return 59;
    				case 3: return 60;
    				case 4: return 61;
    				case 5: return 62;
    				case 6: return 63;
    				case 7: return 64;
    				case 8: return 65;
    				case 9: return 66;
    				case 10: return 67;
    				case 11: return 68;
    				case 12: return 69;
    				case 13: return 70;
    				case 14: return 71;
    				case 15: return 72;
    				case 16: return 73;
    				case 17: return 74;
    				case 18: return 75;
    				case 19: return 76;
    				default: return MISSING_LED;
    			}
    			break;
    		case 6:
    			switch(x){
    				case 0: return 91;
    				case 1: return 90;
    				case 2: return 89;
    				case 3: return 88;
    				case 4: return 87;
    				case 5: return 86;
    				case 6: return 85;
    				case 7: return 84;
    				case 8: return 83;
    				case 9: return 82;
    				case 10: return 81;
    				case 11: return 80;
    				case 12: return 79;
    				case 13: return 78;
    				case 14: return 77;
    				default: return MISSING_LED;
    			}
    			break;
    		case 7:
    			switch(x){
    				case 0: return 92;
    				case 1: return 93;
    				case 2: return 94;
    				case 3: return 95;
    				case 4: return 96;
    				case 10: return 127;
    				case 11: return 128;
    				case 12: return 129;
    				case 13: return 130;
    				case 14: return 131;
    				default: return MISSING_LED;
    			}
    			break;
    		case 8:
    			switch(x){
    				case 0: return 101;
    				case 1: return 100;
    				case 2: return 99;
    				case 3: return 98;
    				case 4: return 97;
    				case 10: return 136;
    				case 11: return 135;
    				case 12: return 134;
    				case 13: return 133;
    				case 14: return 132;
    				default: return MISSING_LED;
    			}
    			break;
    		case 9:
    			switch(x){
    				case 0: return 102;
    				case 1: return 103;
    				case 2: return 104;
    				case 3: return 105;
    				case 4: return 106;
    				case 10: return 137;
    				case 11: return 138;
    				case 12: return 139;
    				case 13: return 140;
    				case 14: return 141;
    				default: return MISSING_LED;
    			}
    			break;
    		case 10:
    			switch(x){
    				case 0: return 111;
    				case 1: return 110;
    				case 2: return 109;
    				case 3: return 108;
    				case 4: return 107;
    				case 10: return 146;
    				case 11: return 145;
    				case 12: return 144;
    				case 13: return 143;
    				case 14: return 142;
    				default: return MISSING_LED;
    			}
    			break;
    		case 11:
    			switch(x){
    				case 0: return 112;
    				case 1: return 113;
    				case 2: return 114;
    				case 3: return 115;
    				case 4: return 116;
    				case 10: return 147;
    				case 11: return 148;
    				case 12: return 149;
    				case 13: return 150;
    				case 14: return 151;
    				default: return MISSING_LED;
    			}
    			break;
    		case 12:
    			switch(x){
    				case 0: return 121;
    				case 1: return 120;
    				case 2: return 119;
    				case 3: return 118;
    				case 4: return 117;
    				case 10: return 156;
    				case 11: return 155;
    				case 12: return 154;
    				case 13: return 153;
    				case 14: return 152;
    				default: return MISSING_LED;
    			}
    			break;
    		case 13:
    			switch(x){
    				case 0: return 122;
    				case 1: return 123;
    				case 2: return 124;
    				case 3: return 125;
    				case 4: return 126;
    				case 10: return 157;
    				case 11: return 158;
    				case 12: return 159;
    				case 13: return 160;
    				case 14: return 161;
    				default: return MISSING_LED;
    			}
    			break;
    		case 14:
    			switch(x){
    				case 1: return 162;
    				case 2: return 163;
    				case 3: return 164;
    				case 4: return 165;
    				case 5: return 166;
    				case 6: return 167;
    				case 7: return 168;
    				case 8: return 169;
    				case 9: return 170;
    				case 10: return 171;
    				case 11: return 172;
    				case 12: return 173;
    				case 13: return 174;
    				case 14: return 175;
    				default: return MISSING_LED;
    			}
    			break;
    		case 15:
    			switch(x){
    				case 2: return 192;
    				case 3: return 191;
    				case 4: return 190;
    				case 5: return 189;
    				case 6: return 188;
    				case 7: return 187;
    				case 8: return 186;
    				case 9: return 185;
    				case 10: return 184;
    				case 11: return 183;
    				case 12: return 182;
    				case 13: return 181;
    				case 14: return 180;
    				case 15: return 179;
    				case 16: return 178;
    				case 17: return 177;
    				case 18: return 176;
    				default: return MISSING_LED;
    			}
    			break;
    		case 16:
    			switch(x){
    				case 1: return 193;
    				case 2: return 194;
    				case 3: return 195;
    				case 4: return 196;
    				case 5: return 197;
    				case 6: return 198;
    				case 7: return 199;
    				case 8: return 200;
    				case 9: return 201;
    				case 10: return 202;
    				case 11: return 203;
    				case 12: return 204;
    				case 13: return 205;
    				case 14: return 206;
    				case 15: return 207;
    				case 16: return 208;
    				case 17: return 209;
    				case 18: return 210;
    				default: return MISSING_LED;
    			}
    			break;
    		case 17:
    			switch(x){
    				case 1: return 229;
    				case 2: return 228;
    				case 3: return 227;
    				case 4: return 226;
    				case 5: return 225;
    				case 6: return 224;
    				case 7: return 223;
    				case 8: return 222;
    				case 9: return 221;
    				case 10: return 220;
    				case 11: return 219;
    				case 12: return 218;
    				case 13: return 217;
    				case 14: return 216;
    				case 15: return 215;
    				case 16: return 214;
    				case 17: return 213;
    				case 18: return 212;
    				case 19: return 211;
    				default: return MISSING_LED;
    			}
    			break;
    		case 18:
    			switch(x){
    				case 1: return 230;
    				case 2: return 231;
    				case 3: return 232;
    				case 4: return 233;
    				case 5: return 234;
    				case 6: return 235;
    				case 7: return 236;
    				case 8: return 237;
    				case 9: return 238;
    				case 10: return 239;
    				case 11: return 240;
    				case 12: return 241;
    				case 13: return 242;
    				case 14: return 243;
    				case 15: return 244;
    				case 16: return 245;
    				case 17: return 246;
    				case 18: return 247;
    				case 19: return 248;
    				case 20: return 249;
    				default: return MISSING_LED;
    			}
    			break;
    		case 19:
    			switch(x){
    				case 1: return 262;
    				case 2: return 261;
    				case 3: return 260;
    				case 4: return 259;
    				case 5: return 258;
    				case 6: return 257;
    				case 7: return 256;
    				case 8: return 255;
    				case 9: return 254;
    				case 16: return 253;
    				case 17: return 252;
    				case 18: return 251;
    				case 19: return 250;
    				default: return MISSING_LED;
    			}
    			break;
    		case 20:
    			switch(x){
    				case 2: return 263;
    				case 3: return 264;
    				case 4: return 265;
    				case 5: return 266;
    				case 6: return 267;
    				case 7: return 268;
    				case 8: return 269;
    				default: return MISSING_LED;
    			}
    			break;
    		case 21:
    			switch(x){
    				case 3: return 274;
    				case 4: return 273;
    				case 5: return 272;
    				case 6: return 271;
    				case 7: return 270;
    				default: return MISSING_LED;
    			}
    			break;
    		case 22:
    			switch(x){
    				case 4: return 275;
    				case 5: return 276;
    				case 6: return 277;
    				default: return MISSING_LED;
    			}
    			break;
    	}
    	return MISSING_LED;	// not neccessary but to avoid error



//			if ((tBWidth == 1) && (tBHeight == 1))
//			{
//				// No Blocks, just a Matrix
//	      if (tMWidth < 0)
//	        x = (m_absMWidth - 1) - x;
//		    if (tMHeight < 0)
//	        y = (m_absMHeight - 1) - y;
//	      if (tMType == HORIZONTAL_MATRIX)
//	        return((y * m_absMWidth) + x);
//	      else if (tMType == VERTICAL_MATRIX)
//	        return((x * m_absMHeight) + y);
//	      else if (tMType == HORIZONTAL_ZIGZAG_MATRIX)
//	      {
//	        if (y % 2)
//	          return((((y + 1) * m_absMWidth) - 1) - x);
//	        else
//	          return((y * m_absMWidth) + x);
//	      }
//	      else /* if (tMType == VERTICAL_ZIGZAG_MATRIX) */
//	      {
//	        if (x % 2)
//	          return((((x + 1) * m_absMHeight) - 1) - y);
//	        else
//	          return((x * m_absMHeight) + y);
//	      }
//			}
//			else
//			{
//				// Reverse Block/Matrix X coordinate if needed
//				if ((tBWidth < 0) && (tMWidth < 0))
//					x = (((m_absBWidth - 1) - (x / m_absMWidth)) * m_absMWidth) + ((m_absMWidth - 1) - (x % m_absMWidth));
//				else if (tBWidth < 0)
//					x = (((m_absBWidth - 1) - (x / m_absMWidth)) * m_absMWidth) + (x % m_absMWidth);
//				else if (tMWidth < 0)
//					x = x - ((x % m_absMWidth) * 2) + (m_absMWidth - 1);
//				// Reverse Block/Matrix Y coordinate if needed
//				if ((tBHeight < 0) && (tMHeight < 0))
//					y = (((m_absBHeight - 1) - (y / m_absMHeight)) * m_absMHeight) + ((m_absMHeight - 1) - (y % m_absMHeight));
//				else if(tBHeight < 0)
//					y = (((m_absBHeight - 1) - (y / m_absMHeight)) * m_absMHeight) + (y % m_absMHeight);
//				else if (tMHeight < 0)
//					y = y - ((y % m_absMHeight) * 2) + (m_absMHeight - 1);
//				// Calculate Block base
//	    	uint16_t Base;
//		    if (tBType == HORIZONTAL_BLOCKS)
//		    	Base = (((y / m_absMHeight) * m_absBWidth) + (x / m_absMWidth)) * (m_absMWidth * m_absMHeight);
//		    else if (tBType == VERTICAL_BLOCKS)
//		    	Base = (((x / m_absMWidth) * m_absBHeight) + (y / m_absMHeight)) * (m_absMHeight * m_absMWidth);
//		    else if (tBType == HORIZONTAL_ZIGZAG_BLOCKS)
//		    {
//	        if ((y / m_absMHeight) % 2)
//			    	Base = (((y / m_absMHeight) * m_absBWidth) + ((m_absBWidth - 1) - (x / m_absMWidth))) * (m_absMWidth * m_absMHeight);
//	       	else
//			    	Base = (((y / m_absMHeight) * m_absBWidth) + (x / m_absMWidth)) * (m_absMWidth * m_absMHeight);
//	      }
//	      else /* if (tBType == VERTICAL_ZIGZAG_BLOCKS) */
//	      {
//	        if ((x / m_absMWidth) % 2)
//			    	Base = (((x / m_absMWidth) * m_absBHeight) + ((m_absBHeight - 1) - (y / m_absMHeight))) * (m_absMHeight * m_absMWidth);
//	        else
//			    	Base = (((x / m_absMWidth) * m_absBHeight) + (y / m_absMHeight)) * (m_absMHeight * m_absMWidth);
//	      }
//				// Calculate Matrix offset
//	      if (tMType == HORIZONTAL_MATRIX)
//	        return(Base + ((y % m_absMHeight) * m_absMWidth) + (x % m_absMWidth));
//	      else if (tMType == VERTICAL_MATRIX)
//	        return(Base + ((x % m_absMWidth) * m_absMHeight) + (y % m_absMHeight));
//	      else if (tMType == HORIZONTAL_ZIGZAG_MATRIX)
//	      {
//	        if ((y % m_absMHeight) % 2)
//	          return(Base + ((((y % m_absMHeight) + 1) * m_absMWidth) - 1) - (x % m_absMWidth));
//	        else
//	          return(Base + ((y % m_absMHeight) * m_absMWidth) + (x % m_absMWidth));
//	      }
//	      else /* if (tMType == VERTICAL_ZIGZAG_MATRIX) */
//	      {
//	        if ((x % m_absMWidth) % 2)
//	          return(Base + ((((x % m_absMWidth) + 1) * m_absMHeight) - 1) - (y % m_absMHeight));
//	        else
//	          return(Base + ((x % m_absMWidth) * m_absMHeight) + (y % m_absMHeight));
//	      }
//	    }
    }

    void ShiftLeft(void)
    {
      if ((tBWidth != 1) || (tBHeight != 1))
     	{
				// Blocks, so no optimisation
		    for (int16_t x=1; x<m_Width; ++x)
  			{
				  for (int16_t y=0; y<m_Height; ++y)
      			m_LED[mXY(x - 1, y)] = m_LED[mXY(x, y)];
			  }
			  for (int16_t y=0; y<m_Height; ++y)
     			m_LED[mXY(m_Width - 1, y)] = CRGB(0, 0, 0);
     	}
     	else
      {
				// No Blocks, just a Matrix so optimise a little
        switch (tMType)
        {
          case HORIZONTAL_MATRIX:
            if (tMWidth > 0)
              HPWSL();
            else
              HNWSL();
            break;
          case VERTICAL_MATRIX:
            if (tMWidth > 0)
              VPWSL();
            else
              VNWSL();
            break;
          case HORIZONTAL_ZIGZAG_MATRIX:
            if (tMWidth > 0)
              HZPWSL();
            else
              HZNWSL();
            break;
          case VERTICAL_ZIGZAG_MATRIX:
            if (tMWidth > 0)
              VZPWSL();
            else
              VZNWSL();
            break;
        }
      }
    }

    void ShiftRight(void)
    {
      if ((tBWidth != 1) || (tBHeight != 1))
     	{
				// Blocks, so no optimisation
		    for (int16_t x=m_Width-1; x>=1; --x)
  			{
				  for (int16_t y=0; y<m_Height; ++y)
      			m_LED[mXY(x, y)] = m_LED[mXY(x - 1, y)];
			  }
			  for (int16_t y=0; y<m_Height; ++y)
     			m_LED[mXY(0, y)] = CRGB(0, 0, 0);
     	}
     	else
      {
				// No Blocks, just a Matrix so optimise a little
        switch (tMType)
        {
          case HORIZONTAL_MATRIX:
            if (tMWidth > 0)
              HNWSL();
            else
              HPWSL();
            break;
          case VERTICAL_MATRIX:
            if (tMWidth > 0)
              VNWSL();
            else
              VPWSL();
            break;
          case HORIZONTAL_ZIGZAG_MATRIX:
            if (tMWidth > 0)
              HZNWSL();
            else
              HZPWSL();
            break;
          case VERTICAL_ZIGZAG_MATRIX:
            if (tMWidth > 0)
              VZNWSL();
            else
              VZPWSL();
            break;
        }
      }
    }

    void ShiftDown(void)
    {
      if ((tBWidth != 1) || (tBHeight != 1))
     	{
				// Blocks, so no optimisation
			  for (int16_t y=1; y<m_Height; ++y)
  			{
			    for (int16_t x=0; x<m_Width; ++x)
      			m_LED[mXY(x, y - 1)] = m_LED[mXY(x, y)];
			  }
		    for (int16_t x=0; x<m_Width; ++x)
     			m_LED[mXY(x, m_Height - 1)] = CRGB(0, 0, 0);
     	}
     	else
      {
				// No Blocks, just a Matrix so optimise a little
        switch (tMType)
        {
          case HORIZONTAL_MATRIX:
            if (tMHeight > 0)
              HPHSD();
            else
              HNHSD();
            break;
          case VERTICAL_MATRIX:
            if (tMHeight > 0)
              VPHSD();
            else
              VNHSD();
            break;
          case HORIZONTAL_ZIGZAG_MATRIX:
            if (tMHeight > 0)
              HZPHSD();
            else
              HZNHSD();
            break;
          case VERTICAL_ZIGZAG_MATRIX:
            if (tMHeight > 0)
              VZPHSD();
            else
              VZNHSD();
            break;
        }
      }
    }

    void ShiftUp(void)
    {
      if ((tBWidth != 1) || (tBHeight != 1))
     	{
				// Blocks, so no optimisation
			  for (int16_t y=m_Height-1; y>=1; --y)
  			{
			    for (int16_t x=0; x<m_Width; ++x)
      			m_LED[mXY(x, y)] = m_LED[mXY(x, y - 1)];
			  }
		    for (int16_t x=0; x<m_Width; ++x)
     			m_LED[mXY(x, 0)] = CRGB(0, 0, 0);
     	}
     	else
      {
				// No Blocks, just a Matrix so optimise a little
        switch (tMType)
        {
          case HORIZONTAL_MATRIX:
          	if (tMHeight > 0)
              HNHSD();
            else
              HPHSD();
            break;
          case VERTICAL_MATRIX:
            if (tMHeight > 0)
              VNHSD();
            else
              VPHSD();
            break;
          case HORIZONTAL_ZIGZAG_MATRIX:
            if (tMHeight > 0)
              HZNHSD();
            else
              HZPHSD();
            break;
          case VERTICAL_ZIGZAG_MATRIX:
            if (tMHeight > 0)
              VZNHSD();
            else
              VZPHSD();
            break;
        }
      }
    }

  private:
  	// Optimised functions used by ShiftLeft & ShiftRight in non block mode
    void HPWSL(void)
    {
      uint32_t i = 0;
      for (int16_t y=m_absMHeight; y>0; --y,++i)
      {
        for (uint16_t x=m_absMWidth-1; x>0; --x,++i)
          p_LED[i] = p_LED[i + 1];
        p_LED[i] = CRGB(0, 0, 0);
      }
    }
    void HNWSL(void)
    {
      uint32_t i = m_absMWidth - 1;
      for (int16_t y=m_absMHeight; y>0; --y)
      {
        for (uint16_t x=m_absMWidth-1; x>0; --x,--i)
          p_LED[i] = p_LED[i - 1];
        p_LED[i] = CRGB(0, 0, 0);
        i += ((m_absMWidth * 2) - 1);
      }
    }
    void VPWSL(void)
    {
      uint32_t i = 0;
      uint32_t j = m_absMHeight;
      for (uint16_t x=m_absMWidth-1; x>0; --x)
      {
        for (int16_t y=m_absMHeight; y>0; --y)
          p_LED[i++] = p_LED[j++];
      }
      for (int16_t y=m_absMHeight; y>0; --y)
        p_LED[i++] = CRGB(0, 0, 0);
    }
    void VNWSL(void)
    {
      uint32_t i = (m_absMHeight * m_absMWidth) - 1;
      uint32_t j = i - m_absMHeight;
      for (uint16_t x=m_absMWidth-1; x>0; --x)
      {
        for (int16_t y=m_absMHeight; y>0; --y)
          p_LED[i--] = p_LED[j--];
      }
      for (int16_t y=m_absMHeight; y>0; --y)
        p_LED[i--] = CRGB(0, 0, 0);
    }
    void HZPWSL(void)
    {
      uint32_t i = 0;
      for (int16_t y=m_absMHeight; y>0; y-=2)
      {
        for (uint16_t x=m_absMWidth-1; x>0; --x,++i)
          p_LED[i] = p_LED[i + 1];
        p_LED[i] = CRGB(0, 0, 0);
        i++;
        if (y > 1)
        {
          i += (m_absMWidth - 1);
          for (uint16_t x=m_absMWidth-1; x>0; --x,--i)
            p_LED[i] = p_LED[i - 1];
          p_LED[i] = CRGB(0, 0, 0);
          i += m_absMWidth;
        }
      }
    }
    void HZNWSL(void)
    {
      uint32_t i = m_absMWidth - 1;
      for (int16_t y=m_absMHeight; y>0; y-=2)
      {
        for (uint16_t x=m_absMWidth-1; x>0; --x,--i)
          p_LED[i] = p_LED[i - 1];
        p_LED[i] = CRGB(0, 0, 0);
        if (y > 1)
        {
          i += m_absMWidth;
          for (uint16_t x=m_absMWidth-1; x>0; --x,++i)
            p_LED[i] = p_LED[i + 1];
          p_LED[i] = CRGB(0, 0, 0);
          i += m_absMWidth;
        }
      }
    }
    void VZPWSL(void)
    {
      uint32_t i = 0;
      uint32_t j = (m_absMHeight * 2) - 1;
      for (uint16_t x=m_absMWidth-1; x>0; x-=2)
      {
        for (int16_t y=m_absMHeight; y>0; --y)
          p_LED[i++] = p_LED[j--];
        if (x > 1)
        {
          j += (m_absMHeight * 2);
          for (int16_t y=m_absMHeight; y>0; --y)
            p_LED[i++] = p_LED[j--];
          j += (m_absMHeight * 2);
        }
      }
      for (int16_t y=m_absMHeight; y>0; y--)
        p_LED[i++] = CRGB(0, 0, 0);
    }
    void VZNWSL(void)
    {
      uint32_t i = (m_absMHeight * m_absMWidth) - 1;
      uint32_t j = m_absMHeight * (m_absMWidth - 2);
      for (uint16_t x=m_absMWidth-1; x>0; x-=2)
      {
        for (int16_t y=m_absMHeight; y>0; --y)
          p_LED[i--] = p_LED[j++];
        if (x > 1)
        {
          j -= (m_absMHeight * 2);
          for (int16_t y=m_absMHeight; y>0; --y)
            p_LED[i--] = p_LED[j++];
          j -= (m_absMHeight * 2);
        }
      }
      for (int16_t y=m_absMHeight; y>0; y--)
        p_LED[i--] = CRGB(0, 0, 0);
    }

  	// Optimised functions used by ShiftDown & ShiftUp in non block mode
    void HPHSD(void)
    {
      uint32_t i = 0;
      uint32_t j = m_absMWidth;
      for (uint16_t y=m_absMHeight-1; y>0; --y)
      {
        for (uint16_t x=m_absMWidth; x>0; --x)
          p_LED[i++] = p_LED[j++];
      }
      for (uint16_t x=m_absMWidth; x>0; --x)
        p_LED[i++] = CRGB(0, 0, 0);
    }
    void HNHSD(void)
    {
      uint32_t i = (m_absMWidth * m_absMHeight) - 1;
      uint32_t j = i - m_absMWidth;
      for (uint16_t y=m_absMHeight-1; y>0; --y)
      {
        for (uint16_t x=m_absMWidth; x>0; --x)
          p_LED[i--] = p_LED[j--];
      }
      for (uint16_t x=m_absMWidth; x>0; --x)
        p_LED[i--] = CRGB(0, 0, 0);
    }
    void VPHSD(void)
    {
      uint32_t i = 0;
      for (uint16_t x=m_absMWidth; x>0; --x,++i)
      {
        for (uint16_t y=m_absMHeight-1; y>0; --y,++i)
          p_LED[i] = p_LED[i + 1];
        p_LED[i] = CRGB(0, 0, 0);
      }
    }
    void VNHSD(void)
    {
      uint32_t i = m_absMHeight - 1;
      for (uint16_t x=m_absMWidth; x>0; --x)
      {
        for (uint16_t y=m_absMHeight-1; y>0; --y,--i)
          p_LED[i] = p_LED[i - 1];
        p_LED[i] = CRGB(0, 0, 0);
        i += ((m_absMHeight * 2) - 1);
      }
    }
    void HZPHSD(void)
    {
      uint32_t i = 0;
      uint32_t j = (m_absMWidth * 2) - 1;
      for (uint16_t y=m_absMHeight-1; y>0; y-=2)
      {
        for (uint16_t x=m_absMWidth; x>0; --x)
          p_LED[i++] = p_LED[j--];
        if (y > 1)
        {
          j += (m_absMWidth * 2);
          for (uint16_t x=m_absMWidth; x>0; --x)
            p_LED[i++] = p_LED[j--];
          j += (m_absMWidth * 2);
        }
      }
      for (uint16_t x=m_absMWidth; x>0; x--)
        p_LED[i++] = CRGB(0, 0, 0);
    }
    void HZNHSD(void)
    {
      uint32_t i = (m_absMWidth * m_absMHeight) - 1;
      uint32_t j = m_absMWidth * (m_absMHeight - 2);
      for (uint16_t y=m_absMHeight-1; y>0; y-=2)
      {
        for (uint16_t x=m_absMWidth; x>0; --x)
          p_LED[i--] = p_LED[j++];
        if (y > 1)
        {
          j -= (m_absMWidth * 2);
          for (uint16_t x=m_absMWidth; x>0; --x)
            p_LED[i--] = p_LED[j++];
          j -= (m_absMWidth * 2);
        }
      }
      for (uint16_t x=m_absMWidth; x>0; x--)
        p_LED[i--] = CRGB(0, 0, 0);
    }
    void VZPHSD(void)
    {
      uint32_t i = 0;
      for (uint16_t x=m_absMWidth; x>0; x-=2)
      {
        for (uint16_t y=m_absMHeight-1; y>0; --y,++i)
          p_LED[i] = p_LED[i + 1];
        p_LED[i] = CRGB(0, 0, 0);
        i++;
        if (x > 1)
        {
          i += (m_absMHeight - 1);
          for (uint16_t y=m_absMHeight-1; y>0; --y,--i)
            p_LED[i] = p_LED[i - 1];
          p_LED[i] = CRGB(0, 0, 0);
          i += m_absMHeight;
        }
      }
    }
    void VZNHSD(void)
    {
      uint32_t i = m_absMHeight - 1;
      for (uint16_t x=m_absMWidth; x>0; x-=2)
      {
        for (uint16_t y=m_absMHeight-1; y>0; --y,--i)
          p_LED[i] = p_LED[i - 1];
        p_LED[i] = CRGB(0, 0, 0);
        if (x > 1)
        {
          i += m_absMHeight;
          for (uint16_t y=m_absMHeight-1; y>0; --y,++i)
            p_LED[i] = p_LED[i + 1];
          p_LED[i] = CRGB(0, 0, 0);
          i += m_absMHeight;
        }
      }
    }

};

#endif
