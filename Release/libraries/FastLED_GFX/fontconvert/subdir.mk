################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
..\libraries\FastLED_GFX\fontconvert\fontconvert.c 

C_DEPS += \
.\libraries\FastLED_GFX\fontconvert\fontconvert.c.d 

LINK_OBJ += \
.\libraries\FastLED_GFX\fontconvert\fontconvert.c.o 


# Each subdirectory must supply rules for building sources it contributes
libraries\FastLED_GFX\fontconvert\fontconvert.c.o: ..\libraries\FastLED_GFX\fontconvert\fontconvert.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:\Sloeber\arduinoPlugin\packages\arduino\tools\avr-gcc\7.3.0-atmel3.6.1-arduino5/bin/avr-gcc" -c -g -Os -Wall -Wextra -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects -mmcu=atmega328p -DF_CPU=16000000L -DARDUINO=10802 -DARDUINO_AVR_NANO -DARDUINO_ARCH_AVR     -I"C:\Sloeber\arduinoPlugin\packages\arduino\hardware\avr\1.8.2\cores\arduino" -I"C:\Sloeber\arduinoPlugin\packages\arduino\hardware\avr\1.8.2\variants\eightanaloginputs" -I"C:\Users\andre\Documents\sloeber-workspace\CLED_GIT\libraries\FastLED" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -D__IN_ECLIPSE__=1 "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


