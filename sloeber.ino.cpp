#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2020-03-09 19:07:48

#include "Arduino.h"
#include "Arduino.h"
#include "Math.h"
#include <avr/pgmspace.h>
#include "libraries/FastLED/FastLED.h"
#include "libraries/LEDMatrix/LEDMatrix.h"
#include "libraries/FastLED_GFX/FastLED_GFX.h"

int getRandomColorValue() ;
void progBlingBlingColoring(unsigned int durationMillis, byte nextPart) ;
void progFastBlingBling(unsigned int durationMillis, byte nextPart) ;
void progFullColors(unsigned int durationMillis, byte nextPart, unsigned int del) ;
void progFullColorsWithFading(unsigned int durationMillis, byte nextPart) ;
void progStrobo(unsigned int durationMillis, byte nextPart, unsigned int del, int red, int green, int blue) ;
void progMatrixScanner(unsigned int durationMillis, byte nextPart) ;
void progStern_initialize() ;
void progStern(unsigned int durationMillis, byte nextPart) ;
void progBlack(unsigned int durationMillis, byte nextPart) ;
void progCircles(unsigned int durationMillis, byte nextPart, unsigned int msForChange) ;
void progRandomLines(unsigned int durationMillis, byte nextPart, unsigned int msForChange) ;
void progMovingLines(unsigned int durationMillis, byte nextPart) ;
void progOutline(unsigned int durationMillis, byte nextPart) ;
void progRunningPixel(unsigned int durationMillis, byte nextPart) ;
void progCLED(unsigned int durationMillis, byte nextPart) ;
void switchToSong(byte song) ;
void setupTimer() ;
ISR(TIMER1_COMPA_vect) ;
void setup() ;
void checkIncomingMIDI() ;
void defaultLoop() ;
void LearnToFly() ;
void Castle() ;
void TooClose() ;
void loop() ;

#include "CLED_GIT.ino"


#endif
